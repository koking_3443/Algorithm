# -*- coding: utf-H -*-
# @Time    : 2021/6/25 9:26
# @Author  : Alex
# @File    : solve.py
# @Software: PyCharm
# ☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆
from typing import List


class Solution:
    def countPoints(self, points: List[List[int]], queries: List[List[int]]) -> List[int]:

